# tech-excellence-blog

Замысловатый блоговый сервис

Фронт на vue.js бэк написан на Perl (mojolicious)- регистрация и авторизация пользователя, 
работа с постами (CRUD) - python (django rest framework)

```
   +--------------------------+
   |        Frontend          | 
   +--------------------------+
        |             |
   +------------+ +-----------+
   | Auth API   | | Posts API | 
   +------------+ +-----------+
```

Для простоты компоненты поднимаются через docker compose:


## Запустить сервис 

запустится на http://localhost:8081/

```shell
./run.sh
```

## Запустить тесты

```shell
./test.sh
```