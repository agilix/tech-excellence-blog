package api_service::Controller::Status;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub get_status ($self) {

  $self->render(json => [
      { status => 'ok' }
  ]);

}

1;
