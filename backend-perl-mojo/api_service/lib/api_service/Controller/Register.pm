package api_service::Controller::Register;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Carp qw/confess/;


sub register ($self) {
  my $username = $self->param('username');
  my $password = $self->param('password');

  confess 'req param missing' unless $username && $password;

  my $status = __save_to_database( $username, $password );

  $self->render(json => [
    { status => $status }
  ]);
}

sub __save_to_database {
    my ( $username, $password ) = @_;

    my $timestamp = time();
    my $file      = '/tmp/users.csv';

    my $status = '';

    unless( -e $file ) {
        open my $FH, '>', $file or confess "$!";
        # Проверка на существование юзера - TODO

        # Записываем юзера в CSV
        print $FH "$username\t$password\t$timestamp\n";
        close $FH;

        $status = 'ok';
    }
    else {
        open my $FH, '<', $file or confess "$!";

        while ( <$FH> ) {
            $_ =~ /(\w+)\t(\w+)\t/;

            if ( $1 eq $username ) {
                $status = "error - $1 exists";
            }
        }
        close $FH;

        unless ( $status =~ /error/ ) {
            open my $FH, '>>', $file or confess "$!";
            print $FH "$username\t$password\t$timestamp\n";
            close $FH;

            $status = 'ok';
        }
    }

    return $status;
}

1;
