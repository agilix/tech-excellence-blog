use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('api_service');
$t->post_ok('/auth/register' => form => {username => 'test', password => '1234'})
    ->status_is(200)
    ->json_is([{status => 'ok'}]);

$t->post_ok('/auth/register' => form => {username => 'test', password => '1234'})
    ->status_is(200)
    ->json_is([{status => 'error - test exists'}]);

{
    my $expect = 'ok';
    my $result = api_service::Controller::Register::__save_to_database( 'test123', '123' );

    is $result, $expect;
}

{
    my $expect = 'error - test123 exists';
    my $result = api_service::Controller::Register::__save_to_database( 'test123', '123' );

    is $result, $expect;
}

done_testing();
