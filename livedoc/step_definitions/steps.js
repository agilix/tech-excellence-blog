const { I } = inject();

let state = {};

Given("я открываю главную страницу", () => {
  I.amOnPage("/");
});

Then("я вижу статус сервиса {string} в подвале", (status) => {
  I.see(`Статус: ${status}`);
});
