exports.config = {
  output: './output',
  helpers: {
    "ChaiWrapper" : {
      "require": "codeceptjs-chai"
    },
    Playwright: {
      url: 'http://alfa.blog.sergeylobin.ru',
      show: false,
      browser: 'chromium',
    },
    "Mochawesome": {
      "uniqueScreenshotNames": "true"
    }
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    "mocha": {
      "reporterOptions": {
        "reportDir": "output"
        // "mochaFile": "output/result.xml",
      }
    },
  },
  tests: './*_test.js',
  name: 'livedoc',
  translation: 'ru-RU'
}