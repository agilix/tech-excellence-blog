#!/bin/sh

docker build --platform linux/amd64 -t agilixru/tech-excellence-blog_frontend:latest frontend-js-vue
docker push agilixru/tech-excellence-blog_frontend:latest frontend-js-vue
