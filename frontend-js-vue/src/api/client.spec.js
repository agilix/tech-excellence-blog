import client from "./client";

global.fetch = jest.fn().mockImplementation( () => {
    return Promise.resolve( { ok : true } )
} );

it('должен отправлять запрос на добавление поста на сервер', () => {
    const dummyPost = {
        title : 'Some header',
        text  : 'Some text,'
    }

    client.createPost( dummyPost );

    expect( global.fetch ).toBeCalledWith(
        "/api/blog/posts/create",
        {
            "body": "{\"title\":\"Some header\",\"text\":\"Some text,\"}",
            "headers": {"Content-Type": "application/json;charset=utf-8"},
            "method": "POST"
        } );
} );