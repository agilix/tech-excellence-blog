import {shallowMount} from "@vue/test-utils";
import PostList from "./PostList.vue";
import { getPosts } from '../api/client'

jest.mock("../api/client", () => ({
    getPosts: jest.fn()
}));


describe("PostList", () => {

    it("renders list of posts with titles and texts", async () => {
        getPosts.mockImplementation(() => Promise.resolve([{'id': 1, 'title': "test title", 'text': "test blog text"}]))

        const wrapper = shallowMount(PostList);

        await wrapper.vm.$nextTick();
        await wrapper.vm.$nextTick();

        expect(wrapper.find(".blog-post-title").text()).toBe("test title");
        expect(wrapper.find(".blog-post-text").text()).toBe("test blog text");
    });

    it('post should display editing post date', async () => {
        getPosts.mockImplementation(() => Promise.resolve([{'id': 1, 'title': "test title", "created": "21-12-2021", "edited": "21-12-2021 6:30PM"}]))

        const wrapper = shallowMount(PostList);

        await wrapper.vm.$nextTick();
        await wrapper.vm.$nextTick();

        expect(wrapper.html()).toContain('Последнее редактирования поста: 21-12-2021 6:30PM');
    })

    it("renders list of posts with authors", async () => {
        getPosts.mockImplementation(() => Promise.resolve([{'id': 1, 'author': "test author"}]))

        const wrapper = shallowMount(PostList);
        await wrapper.vm.$nextTick();
        await wrapper.vm.$nextTick();
        expect(wrapper.find(".blog-post-author").text()).toBe("test author");
    });

    it('post should display creation date', async () => {
        getPosts.mockImplementation(() => Promise.resolve([{'id': 1, 'title': "test title", "created": "21-12-2021"}]))

        const wrapper = shallowMount(PostList);

        await wrapper.vm.$nextTick();
        await wrapper.vm.$nextTick();
        
        expect(wrapper.html()).toContain('Дата создания поста: 21-12-2021');
    })

    it('should display no posts message, when post list empty', async () => {
        getPosts.mockImplementation(() => Promise.resolve([]))

        const wrapper = shallowMount(PostList);

        await wrapper.vm.$nextTick();
        await wrapper.vm.$nextTick();

        expect(wrapper.html()).toContain('Постов пока что нет!')
    })
});
