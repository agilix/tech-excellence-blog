from django.urls import path
from . import views

urlpatterns = [
    path('status/', views.Status.as_view(), name='get_status'),
    path('posts/', views.PostList.as_view(), name='get_posts_list'),
    path('posts/create', views.PostCreate.as_view(), name='post_add_post')
]
