# Generated by Django 3.2.9 on 2022-02-12 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_merge_0002_post_edited_0002_post_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='author',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
    ]
